#!/bin/bash
set -u

CURRENT_DIR=$(dirname "$0")
export WORK_DIR=`echo $(cd $(dirname "$0")/..; pwd)`

docker-compose -f ${WORK_DIR}/docker/docker-compose.yml up -d mongodb