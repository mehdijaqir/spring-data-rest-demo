package com.mjg.demo.repositories

import com.mjg.demo.model.DemoCharacter
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource(collectionResourceRel = "characters", path = "characters")
interface CharacterRepository : MongoRepository<DemoCharacter, String> {
    fun findByFirstName(name: String): DemoCharacter
}