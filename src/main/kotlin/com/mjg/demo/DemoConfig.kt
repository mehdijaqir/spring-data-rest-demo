package com.mjg.demo

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import org.springframework.data.domain.AuditorAware
import org.springframework.data.mongodb.config.EnableMongoAuditing
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean
import java.util.*

@Configuration
@EnableMongoAuditing
class DemoConfig {

    @Bean
    fun repositoryPopulator(): Jackson2RepositoryPopulatorFactoryBean {
        val factoryBean = Jackson2RepositoryPopulatorFactoryBean()
        factoryBean.setResources(arrayOf<Resource>(ClassPathResource("characters.json")))
        return factoryBean
    }

    @Bean
    fun auditor(): AuditorAware<String> = AuditorAware {
        Optional.of("DemoApp-User")
    }
}