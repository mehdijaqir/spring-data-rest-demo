package com.mjg.demo.ctrl

import com.mjg.demo.model.About
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.apache.commons.lang3.StringUtils.defaultIfEmpty
import java.net.URL
import java.util.jar.Manifest


@RestController
class HomeController {

    val manifest by lazy {
        val classPath = HomeController::class.java.getResource(HomeController::class.java.simpleName + ".class").toString()
        var result = emptyMap<Any, Any>()
        if (classPath.startsWith("jar")) {
            try {
                result = Manifest(URL(classPath.substring(0, classPath.lastIndexOf('!') + 1) + "/META-INF/MANIFEST.MF").openStream()).mainAttributes.toMap()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }
        result
    }

    @GetMapping("/about")
    fun about() = About(authors = mapOf("Mehdi" to "DevOps", "Charlie" to "Auditor"), properties = manifest)
}