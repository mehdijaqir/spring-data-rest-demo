package com.mjg.demo.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime
import java.util.*

data class About(val appName: String = "DemoApp", val authors: Map<String, String>, val properties: Map<Any, Any>)

@Document
data class DemoCharacter @JsonCreator constructor(@JsonProperty("id") @Id var id: String? = null,
                                                  @LastModifiedBy @JsonProperty("lastModifiedBy") var lastModifiedBy: String? = null,
                                                  @LastModifiedDate @JsonProperty("lastModifiedDate") var lastModifiedDate: LocalDateTime? = null,
                                                  @JsonProperty("firstName") var firstName: String?,
                                                  @JsonProperty("lastName") var lastName: String?,
                                                  @JsonProperty("age") var age: Int? = 0)