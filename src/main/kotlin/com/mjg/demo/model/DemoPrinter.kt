package com.mjg.demo.model

import com.mjg.demo.repositories.CharacterRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.ansi.AnsiColor
import org.springframework.boot.ansi.AnsiOutput
import org.springframework.stereotype.Component

@Component
class DemoPrinter(@Autowired val characterRepository: CharacterRepository): CommandLineRunner {

    override fun run(vararg args: String?) {
        writeToConsole(this.characterRepository.findAll())
    }

    internal fun writeToConsole(objects: Iterable<Any>) {
        println()
        writeSeparator()
        objects.forEach { writeLine(it) }
        writeSeparator()
        println()
    }

    internal fun writeSeparator() {
        println(AnsiOutput.toString(AnsiColor.GREEN, "  ###########################################################################################################"))
    }

    internal fun writeLine(obj: Any) {
        print(AnsiOutput.toString(AnsiColor.GREEN, "  ##  ") + AnsiOutput.toString(AnsiColor.YELLOW, obj))
        println()
    }
}