package com.mjg.demo

import com.mjg.demo.model.DemoCharacter
import com.mjg.demo.model.DemoPrinter
import com.mjg.demo.repositories.CharacterRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class CharactersIntegrationTests @Autowired constructor(var characterRepository: CharacterRepository,
                                                        var demoPrinter: DemoPrinter) : AbstractIntegrationTests() {

    @Test
    fun `Get character by first name`() {
        with(client.getForEntity("/characters/search/findByFirstName?name=Cartman", DemoCharacter::class.java)) {
            assertThat(this.statusCodeValue).isEqualTo(200)
            assertThat(this.body).isNotNull().extracting("firstName", "lastName").containsExactly("Cartman", "Eric")
        }
    }

    @Test
    fun `Add new character`() {
        val count = characterRepository.count()
        with(client.postForEntity("/characters", DemoCharacter(firstName = "McCormick", lastName = "Kenny", age = 10), DemoCharacter::class.java)) {
            assertThat(this.statusCodeValue).isEqualTo(201)
            assertThat(this.body).isNotNull().extracting("firstName", "lastName").containsExactly("McCormick", "Kenny")
        }
        assertThat(characterRepository.count()).isEqualTo(count + 1)
        demoPrinter.writeToConsole(characterRepository.findAll())
    }
}